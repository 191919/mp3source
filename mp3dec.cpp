/*
 * MP3Source - DirectShow MP3 Source Filter
 * Copyright (C) 2007 6rooms.com
 * Written by jh@6rooms.com.
 *
 * This file, mp3dec.cpp, is based on CoolPlayer's source file CPI_Player_CoDec_MPEG.c.
 *
 * CoolPlayer - Blazing fast audio player.
 * Copyright (C) 2000-2004 Niek Albers.
 * Released under the GNU General Public License.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <windows.h>
#include "mp3dec.h"
#pragma warning(disable: 4127 4244)

static int parse_xing(struct xing *xing, struct mad_bitptr ptr, unsigned int bitlen)
{
    if (bitlen < 64 || mad_bit_read(&ptr, 32) != XING_MAGIC)
        goto fail;

    xing->flags = mad_bit_read(&ptr, 32);
    bitlen -= 64;

    if (xing->flags & XING_FRAMES)
	{
        if (bitlen < 32)
            goto fail;

        xing->frames = mad_bit_read(&ptr, 32);
        bitlen -= 32;
    }

    if (xing->flags & XING_BYTES)
	{
        if (bitlen < 32)
            goto fail;

        xing->bytes = mad_bit_read(&ptr, 32);
        bitlen -= 32;
    }

    if (xing->flags & XING_TOC)
	{
        int i;

        if (bitlen < 800)
            goto fail;

        for (i = 0; i < 100; ++i)
            xing->toc[i] = (unsigned char) mad_bit_read(&ptr, 8);

        bitlen -= 800;
    }

    if (xing->flags & XING_SCALE)
	{
        if (bitlen < 32)
            goto fail;

        xing->scale = mad_bit_read(&ptr, 32);
        bitlen -= 32;
    }

    return 0;

fail:
    xing->flags = 0;
    return -1;
}

static __inline BOOL StreamClose(HANDLE h)
{
	return CloseHandle(h);
}

static __inline HANDLE StreamOpen(LPCTSTR fn)
{
	return CreateFile(fn, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_ALWAYS, 0, NULL);
}

static __inline BOOL StreamRead(HANDLE h, void* pDestBuffer, const DWORD iBytesToRead, DWORD* piBytesRead)
{
	return ReadFile(h, pDestBuffer, iBytesToRead, piBytesRead, 0);
}

static __inline BOOL StreamSeek(HANDLE h, const DWORD iNewOffset)
{
	return SetFilePointer(h, iNewOffset, 0, FILE_BEGIN);
}

static __inline DWORD StreamGetLength(HANDLE h)
{
	return GetFileSize(h, NULL);
}

static __inline DWORD StreamTell(HANDLE h)
{
	return SetFilePointer(h, 0, NULL, FILE_CURRENT);
}

static int scan_header(HANDLE h, struct mad_header *header, struct xing *xing)
{
    struct mad_stream stream;
    struct mad_frame frame;
    unsigned char buffer[8192];
    unsigned int buflen = 0;
    int count = 0, result = 0;

    mad_stream_init(&stream);
    mad_frame_init(&frame);

    if (xing)
        xing->flags = 0;

    while (1)
	{
        if (buflen < sizeof(buffer))
		{
            DWORD bytes;

            if(StreamRead(h, buffer + buflen, sizeof(buffer) - buflen, &bytes) == FALSE
                    || bytes == 0)
            {
                result = -1;
                break;
            }

            buflen += bytes;
        }

        mad_stream_buffer(&stream, buffer, buflen);

        while (1)
		{
            if (mad_frame_decode(&frame, &stream) == -1)
			{
                if (!MAD_RECOVERABLE(stream.error))
                    break;

                continue;
            }

            if (count++ || (xing && parse_xing(xing, stream.anc_ptr, stream.anc_bitlen) == 0))
                break;
        }

        if (count || stream.error != MAD_ERROR_BUFLEN)
            break;

        memmove(buffer, stream.next_frame, buflen = &buffer[buflen] - stream.next_frame);
    }

    if (count)
	{
        if (header)
            *header = frame.header;
    }
    else
        result = -1;

    mad_frame_finish(&frame);
    mad_stream_finish(&stream);

    return result;
}

static __inline unsigned long prng(unsigned long state)
{
    return (state * 0x0019660dL + 0x3c6ef35fL) & 0xffffffffL;
}

static signed int dither(mad_fixed_t sample, struct dither *dither)
{
    unsigned int scalebits;
    mad_fixed_t output, mask, random;

    enum { MIN = -MAD_F_ONE, MAX =  MAD_F_ONE - 1 };

    /* noise shape */
    sample += dither->error[0] - dither->error[1] + dither->error[2];

    dither->error[2] = dither->error[1];
    dither->error[1] = dither->error[0] / 2;

    /* bias */
    output = sample + (1L << (MAD_F_FRACBITS + 1 - SAMPLE_DEPTH - 1));

    scalebits = MAD_F_FRACBITS + 1 - SAMPLE_DEPTH;
    mask = (1L << scalebits) - 1;

    /* dither */
    random  = prng(dither->random);
    output += (random & mask) - (dither->random & mask);

    dither->random = random;

    /* clip */
    if (output > MAX)
	{
        output = MAX;

        if (sample > MAX)
            sample = MAX;
    }
    else if (output < MIN)
	{
        output = MIN;

        if (sample < MIN)
            sample = MIN;
    }

    /* quantize */
    output &= ~mask;

    /* error feedback */
    dither->error[0] = sample - output;

    /* scale */
    return output >> scalebits;
}

static void pack_pcm(unsigned char **pcm, unsigned int nsamples, mad_fixed_t const *ch1, mad_fixed_t const *ch2)
{
    register signed int s0, s1;
    static struct dither d0, d1;

    if (ch2)
	{
		/* stereo */
        while (nsamples--)
		{
            s0 = scale(*ch1++, &d0);
            s1 = scale(*ch2++, &d1);
# if SAMPLE_DEPTH == 16
            (*pcm)[0 + 0] = s0 >> 0;
            (*pcm)[0 + 1] = s0 >> 8;
            (*pcm)[2 + 0] = s1 >> 0;
            (*pcm)[2 + 1] = s1 >> 8;

            *pcm += 2 * 2;
# elif SAMPLE_DEPTH == 8
            (*pcm)[0] = s0 ^ 0x80;
            (*pcm)[1] = s1 ^ 0x80;

            *pcm += 2;
# else
#  error "bad SAMPLE_DEPTH"
# endif
        }
    }
    else
	{
		/* mono */
        while (nsamples--)
		{
            s0 = scale(*ch1++, &d0);

# if SAMPLE_DEPTH == 16
            (*pcm)[0] = s0 >> 0;
            (*pcm)[1] = s0 >> 8;

            *pcm += 2;
# elif SAMPLE_DEPTH == 8
            *(*pcm)++ = s0 ^ 0x80;
# endif
        }
    }
}

static void cleanup(CPs_CoDec_MPEG *context)
{
	if (context->h != INVALID_HANDLE_VALUE)
	{
		StreamClose(context->h);
		mad_synth_finish(&context->synth);
		mad_frame_finish(&context->frame);
		mad_stream_finish(&context->stream);
	}
	memset(context, 0, sizeof(*context));
}

BOOL MP3_OpenFile(CPs_CoDec_MPEG *context, LPCTSTR path)
{
    int iStreamStart = 0;

    cleanup(context);

    context->h = StreamOpen(path);
    if (context->h == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }

	CIs_ID3v2Tag tag;
	DWORD iBytesRead;

	memset(&tag, 0, sizeof(tag));
	StreamRead(context->h, &tag, sizeof(tag), &iBytesRead);

	if(memcmp(tag.m_cTAG, "ID3", 3) == 0)
	{
		iStreamStart = sizeof(CIs_ID3v2Tag);
		iStreamStart += (tag.m_cSize_Encoded[0] << 21)
			| (tag.m_cSize_Encoded[1] << 14)
			| (tag.m_cSize_Encoded[2] << 7)
			| tag.m_cSize_Encoded[3];
	}

	StreamSeek(context->h, iStreamStart);

    mad_stream_init(&context->stream);
    mad_frame_init(&context->frame);
    mad_synth_init(&context->synth);

    if (scan_header(context->h, &context->frame.header, &context->xing) == -1)
	{
        cleanup(context);
        return FALSE;
    }

    StreamSeek(context->h, iStreamStart);

    context->size             = StreamGetLength(context->h);
    context->synth.pcm.length = 0;
    context->samplecount      = 0;
    context->timer            = mad_timer_zero;

    if (context->xing.flags & XING_FRAMES)
	{
        context->length = context->frame.header.duration;
        mad_timer_multiply(&context->length, context->xing.frames);
    }
    else
	{
        /* estimate playing time from file size */
        mad_timer_set(&context->length, 0, 1, context->frame.header.bitrate / 8);
        mad_timer_multiply(&context->length, context->size);
    }

    context->rate   = 0;
    context->frames = 0;
    context->info.m_iFileLength_Secs = mad_timer_count(context->length, MAD_UNITS_SECONDS);
    context->info.m_iBitRate_Kbs     = context->frame.header.bitrate / 1000;
    context->info.m_iFreq_Hz         = context->frame.header.samplerate;
    context->info.m_bStereo          = context->frame.header.mode == MAD_MODE_SINGLE_CHANNEL ? FALSE : TRUE;
    context->info.m_b16bit           = (SAMPLE_DEPTH == 16);
    context->buflen = 0;

    return TRUE;
}

void MP3_CloseFile(CPs_CoDec_MPEG *context)
{
    cleanup(context);
}

void MP3_Seek(CPs_CoDec_MPEG *context, int const numer, int const denom)
{
    double fraction;
    DWORD position;

    fraction = (double) numer / denom;
    position = (DWORD)(mad_timer_count(context->length, MAD_UNITS_MILLISECONDS) * fraction);

    mad_timer_set(&context->timer, position / 1000, position % 1000, 1000);

    if (context->xing.flags & XING_TOC)
	{
        int percent, p1, p2;

        percent = (int) (fraction * 100);
        p1 = (percent < 100) ? context->xing.toc[percent    ] : 0x100;
        p2 = (percent <  99) ? context->xing.toc[percent + 1] : 0x100;

        fraction = (p1 + (p2 - p1) * (fraction * 100 - percent)) / 0x100;
    }

    StreamSeek(context->h, (LONG) (context->size * fraction));
    if (StreamRead(context->h, context->buffer, sizeof(context->buffer), &context->buflen) == FALSE)
	{
        context->buflen = 0;
	}

    mad_stream_buffer(&context->stream, context->buffer, context->buflen);

    mad_frame_mute(&context->frame);
    mad_synth_mute(&context->synth);

    if (numer)
	{
        int skip;

        skip = 2;
        do
		{
            if (mad_frame_decode(&context->frame, &context->stream) == 0)
			{
                mad_timer_add(&context->timer, context->frame.header.duration);
                if (--skip == 0)
                    mad_synth_frame(&context->synth, &context->frame);
            }
            else if (!MAD_RECOVERABLE(context->stream.error))
                break;
        }
        while (skip);
    }

    context->synth.pcm.length = 0;
    context->samplecount      = 0;
}

BOOL MP3_GetPCMBlock(CPs_CoDec_MPEG *context, void *block, DWORD *size)
{
    unsigned char *samples = (unsigned char*) block;
    unsigned int nsamples;

    nsamples = (*size / (SAMPLE_DEPTH / 8)) >> (context->info.m_bStereo ? 1 : 0);
    *size = 0;

    while (nsamples)
	{
        unsigned int count, bitrate;

        count = context->synth.pcm.length - context->samplecount;
        if (count > nsamples)
            count = nsamples;

        if (count)
		{
            mad_fixed_t const *ch1, *ch2;

            ch1 = context->synth.pcm.samples[0] + context->samplecount;
            ch2 = context->synth.pcm.samples[1] + context->samplecount;

            if (context->info.m_bStereo == FALSE)
                ch2 = 0;
            else if (context->synth.pcm.channels == 1)
                ch2 = ch1;

            pack_pcm(&samples, count, ch1, ch2);

            context->samplecount += count;
            nsamples             -= count;

            if (nsamples == 0)
                break;
        }

        while (mad_frame_decode(&context->frame, &context->stream) == -1)
		{
            DWORD bytes;

            if (MAD_RECOVERABLE(context->stream.error))
                continue;

            if (context->stream.next_frame)
			{
                memmove(context->buffer, context->stream.next_frame,
                        context->buflen = context->buffer +
                                          context->buflen - context->stream.next_frame);
            }

            if (StreamRead(context->h, context->buffer + context->buflen, sizeof(context->buffer) - context->buflen, &bytes) == FALSE || bytes == 0)
            {
                return FALSE;
            }

            mad_stream_buffer(&context->stream, context->buffer, context->buflen += bytes);

        }

        bitrate = context->frame.header.bitrate / 1000;

        context->rate += bitrate;
        context->frames++;

        context->info.m_iBitRate_Kbs = bitrate;

        mad_synth_frame(&context->synth, &context->frame);

        context->samplecount = 0;

        mad_timer_add(&context->timer, context->frame.header.duration);
    }

    *size = samples - (unsigned char *) block;

    return TRUE;
}

void MP3_GetFileInfo(CPs_CoDec_MPEG *context, CPs_FileInfo *info)
{
    if (!(context->xing.flags & XING_FRAMES) && context->frames)
	{
        /* update length estimate */
        mad_timer_set(&context->length, 0, 1, (context->rate / context->frames) * (1000 / 8));
        mad_timer_multiply(&context->length, context->size);

        if (mad_timer_compare(context->timer, context->length) > 0)
        {
            context->length = context->timer;
            context->size   = StreamGetLength(context->h);
        }

        context->info.m_iFileLength_Secs = mad_timer_count(context->length, MAD_UNITS_SECONDS);
    }

    *info = context->info;
}

int MP3_GetCurrentPos(CPs_CoDec_MPEG *context)
{
    return mad_timer_count(context->timer, MAD_UNITS_SECONDS);
}