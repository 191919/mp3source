/*
 * MP3Source - DirectShow MP3 Source Filter
 * Copyright (C) 2007 6rooms.com
 * Written by jh@6rooms.com.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <streams.h>
#include <olectl.h>
#include <initguid.h>
#include <tchar.h>
#include "MP3Source.h"
#include "mp3dec.h"
#pragma warning(disable: 4710 4355)

// Setup data

const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
	&MEDIATYPE_Audio,		// Major type
	&MEDIASUBTYPE_PCM		// Minor type
};

const AMOVIESETUP_PIN sudOpPin =
{
	L"Wave Out",			// Pin string name
	FALSE,					// Is it rendered
	TRUE,					// Is it an output
	FALSE,					// Can we have none
	FALSE,					// Can we have many
	&CLSID_NULL,			// Connects to filter
	NULL,					// Connects to pin
	1,						// Number of types
	&sudOpPinTypes			// Pin details
};

const AMOVIESETUP_FILTER sudFilter =
{
	&CLSID_MP3Source,		// Filter CLSID
	L"6rooms MP3 Source",	// String name
	MERIT_DO_NOT_USE,		// Filter merit
	1,						// Number pins
	&sudOpPin				// Pin details
};

CFactoryTemplate g_Templates[] =
{
	{ L"6rooms MP3 Source", &CLSID_MP3Source, CMP3Source::CreateInstance, NULL, &sudFilter }
};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);

//
// (un)Register as .mp3 source filter
// Taken from Delphi MP3Source 1.30
//
void RegisterSourceFilter(LPOLESTR iid)
{
	HKEY hKey;
	static TCHAR szDefaultFilter[] = { TEXT("{E436EBB5-524F-11CE-9F53-0020AF0BA770}") };
	static TCHAR szMediaType[] = { TEXT("{E436EB83-524F-11CE-9F53-0020AF0BA770}") };
	static TCHAR szSubtype[] = { TEXT("{E436EB87-524F-11CE-9F53-0020AF0BA770}") };

	if (RegCreateKeyEx(HKEY_CLASSES_ROOT, TEXT("Media Type\\Extensions\\.mp3"), NULL, NULL, 0, KEY_WRITE, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		if (iid)
			RegSetValueExW(hKey, TEXT("Source Filter"), NULL, REG_SZ, (LPBYTE) iid, (wcslen(iid) + 1) * sizeof(OLECHAR));
		else
			RegSetValueExW(hKey, TEXT("Source Filter"), NULL, REG_SZ, (LPBYTE) szDefaultFilter, sizeof(szDefaultFilter));
		RegSetValueEx(hKey, TEXT("Media Type"), NULL, REG_SZ, (LPBYTE) szMediaType, sizeof(szMediaType));
		RegSetValueEx(hKey, TEXT("Subtype"), NULL, REG_SZ, (LPBYTE) szSubtype, sizeof(szSubtype));
		RegCloseKey(hKey);
	}
}

STDAPI DllRegisterServer()
{
	OLECHAR iid[256];
	StringFromGUID2(CLSID_MP3Source, iid, 256);
	RegisterSourceFilter(iid);
	return AMovieDllRegisterServer2(TRUE);
}

STDAPI DllUnregisterServer()
{
	RegisterSourceFilter(NULL);
	return AMovieDllRegisterServer2(FALSE);
}

STDMETHODIMP CMP3Source::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
	CheckPointer(ppv, E_POINTER);

	if (riid == IID_IFileSourceFilter)
	{
		return GetInterface((IFileSourceFilter*) this, ppv);
	}
	else if (riid == IID_IAMFilterMiscFlags)
	{
		return GetInterface((IAMFilterMiscFlags*) this, ppv);
	}
	return CSource::NonDelegatingQueryInterface(riid, ppv);
}

CUnknown* WINAPI CMP3Source::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
	CUnknown* punk = new CMP3Source(lpunk, phr);
	if (punk == NULL)
	{
		if (phr) *phr = E_OUTOFMEMORY;
	}
	return punk;
}

CMP3Source::CMP3Source(LPUNKNOWN lpunk, HRESULT *phr) :
	CSource(NAME("MP3 Source"), lpunk, CLSID_MP3Source)
{
}

HRESULT CMP3Source::Load(LPCOLESTR pszFileName, const AM_MEDIA_TYPE *pmt)
{
	if (GetPinCount() > 0)
	{
		return VFW_E_ALREADY_CONNECTED;
	}
	CAutoLock cAutoLock(&m_cStateLock);
	wcscpy(m_szFileName, pszFileName);

	m_paStreams = (CSourceStream**) new CMP3Stream*[1];
	if (m_paStreams == NULL)
	{
		return E_OUTOFMEMORY;
	}

	HRESULT hr = NOERROR;
	m_paStreams[0] = new CMP3Stream(&hr, this, L"Wave Out", m_szFileName);
	if (m_paStreams[0] == NULL)
	{
		return E_OUTOFMEMORY;
	}
	if (FAILED(hr))
	{
		return hr;
	}
	return NOERROR;
}

HRESULT CMP3Source::GetCurFile(LPOLESTR *ppszFileName, AM_MEDIA_TYPE *pmt)
{
	CheckPointer(ppszFileName, E_POINTER);
	*ppszFileName = (LPOLESTR) CoTaskMemAlloc((wcslen(m_szFileName) + 1) * sizeof(WCHAR));
	wcscpy(*ppszFileName, m_szFileName);
	return NOERROR;
}

/////////////////////////////////////////////////////////////

CMP3Stream::CMP3Stream(HRESULT *phr, CMP3Source *pParent, LPCWSTR pPinName, LPCWSTR pszFileName)
	: CSourceStream(NAME("MP3 Source"), phr, pParent, pPinName)
	, CSourceSeeking(NAME("MP3 Source"), (IPin*) this, phr, &m_cSharedState)
	, m_pszFileName(pszFileName)
	, m_bDiscontinuity(FALSE)
	, m_bFlushing(FALSE)
{
	ASSERT(phr);
	CAutoLock cAutoLock(&m_cSharedState);

	memset(&m_mp3, 0, sizeof(m_mp3));

	if (MP3_OpenFile(&m_mp3, m_pszFileName) == FALSE)
	{
		*phr = E_INVALIDARG;
		return;
	}

	m_rtStart = m_rtPosition = 0;
	m_rtDuration = m_rtStop = 10000000i64 * m_mp3.info.m_iFileLength_Secs;
}

CMP3Stream::~CMP3Stream()
{
}

STDMETHODIMP CMP3Stream::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
	CheckPointer(ppv, E_POINTER);

	if (riid == IID_IMediaSeeking)
	{
		return CSourceSeeking::NonDelegatingQueryInterface(riid, ppv);
	}
	return CSourceStream::NonDelegatingQueryInterface(riid, ppv);
}

HRESULT CMP3Stream::FillBuffer(IMediaSample *pms)
{
	CheckPointer(pms, E_POINTER);

	BYTE *pData;
	long lDataLen;

	pms->GetPointer(&pData);
	lDataLen = pms->GetSize();

	ZeroMemory(pData, lDataLen);
	{
		CAutoLock cAutoLockShared(&m_cSharedState);
		DWORD len = lDataLen;
		if (MP3_GetPCMBlock(&m_mp3, pData, &len) == FALSE)
		{
			len = lDataLen = 0;
		}
		pms->SetActualDataLength(len);

		// bytes -> duration
		int nChannels = m_mp3.info.m_bStereo ? 2 : 1;
		int nBytesPerSample = m_mp3.info.m_b16bit ? 2 : 1;
		LONGLONG rtEnd = m_rtPosition + 10000000i64 * len / m_mp3.info.m_iFreq_Hz / nChannels / nBytesPerSample;
		pms->SetMediaTime(&m_rtPosition, &rtEnd);
		pms->SetTime(&m_rtPosition, &rtEnd);
		m_rtPosition = rtEnd;
	}

	//
	// We have reached EOF if MP3_GetPCMBlock returns 0 as len
	//
	if (lDataLen == 0)
	{
		return S_FALSE;
	}

	pms->SetSyncPoint(TRUE);
	return NOERROR;
}

HRESULT CMP3Stream::GetMediaType(int iPosition, CMediaType *pmt)
{
	CAutoLock cAutoLock(m_pFilter->pStateLock());

	if (iPosition < 0) return E_INVALIDARG;
	if (iPosition > 0) return VFW_S_NO_MORE_ITEMS;

	//
	// The only format we support
	//

	int nChannels = m_mp3.info.m_bStereo ? 2 : 1;
	int nBytesPerSample = m_mp3.info.m_b16bit ? 2 : 1;

	pmt->SetType(&MEDIATYPE_Audio);
	pmt->SetSubtype(&MEDIASUBTYPE_PCM);
	pmt->SetFormatType(&FORMAT_WaveFormatEx);
	pmt->SetSampleSize(nChannels * nBytesPerSample);
	pmt->SetTemporalCompression(FALSE);

	WAVEFORMATEX* pwf = (WAVEFORMATEX*) pmt->AllocFormatBuffer(sizeof(WAVEFORMATEX));
	memset(pwf, 0, sizeof(*pwf));
	pwf->wFormatTag = WAVE_FORMAT_PCM;
	pwf->nChannels = (WORD) nChannels;
	pwf->nSamplesPerSec = m_mp3.info.m_iFreq_Hz;
	pwf->wBitsPerSample = (WORD) (8 * nBytesPerSample);
	pwf->nBlockAlign = (WORD) (nChannels * nBytesPerSample);
	pwf->nAvgBytesPerSec = m_mp3.info.m_iFreq_Hz * pwf->nBlockAlign;
	pwf->cbSize = 0;

	return NOERROR;
}

HRESULT CMP3Stream::DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *pProperties)
{
	CheckPointer(pAlloc, E_POINTER);
	CheckPointer(pProperties, E_POINTER);

	CAutoLock cAutoLock(m_pFilter->pStateLock());

	// Don't allocate big buffers. This will result in a drunken video frame rate.
	pProperties->cBuffers = 1;
	pProperties->cbBuffer = m_mp3.info.m_iFreq_Hz / 10;

	ALLOCATOR_PROPERTIES Actual;
	HRESULT hr = pAlloc->SetProperties(pProperties, &Actual);
	if (FAILED(hr))
	{
		return hr;
	}

	if (Actual.cbBuffer < pProperties->cbBuffer)
	{
		return E_FAIL;
	}

	ASSERT (Actual.cBuffers == 1);
	return NOERROR;
}

HRESULT CMP3Stream::ChangeStart()
{
	{
		CAutoLock lock(CSourceSeeking::m_pLock);
		m_rtPosition = m_rtStart;

		int n1 = (int) (m_rtPosition / 10000000i64);
		int n2 = (int) (m_rtDuration / 10000000i64);
		MP3_Seek(&m_mp3, n1, n2);
	}
	UpdateFromSeek();
	return S_OK;
}

HRESULT CMP3Stream::ChangeStop()
{
	{
		CAutoLock lock(CSourceSeeking::m_pLock);
		if (m_rtPosition < m_rtStop)
			return S_OK;
	}
	UpdateFromSeek();
	return S_OK;
}

void CMP3Stream::UpdateFromSeek()
{
	if (ThreadExists())
	{
		m_bFlushing = TRUE;
		DeliverBeginFlush();
		Stop();
		DeliverEndFlush();
		m_bFlushing = FALSE;
		Run();
	}
}