/*
 * MP3Source - DirectShow MP3 Source Filter
 * Copyright (C) 2007 6rooms.com
 * Written by jh@6rooms.com.
 *
 * This file, mp3dec.h, is based on CoolPlayer's source file CPI_Player_CoDec_MPEG.c.
 *
 * CoolPlayer - Blazing fast audio player.
 * Copyright (C) 2000-2004 Niek Albers.
 * Released under the GNU General Public License.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#pragma once

#include <libmad/mad.h>

# define SAMPLE_DEPTH	16
# define scale(x, y)	dither((x), (y))

typedef struct _CPs_FileInfo
{
	UINT m_iFileLength_Secs;
	UINT m_iBitRate_Kbs;
	UINT m_iFreq_Hz;
	BOOL m_bStereo;
	BOOL m_b16bit;
} CPs_FileInfo;

#pragma pack(push, 1)
typedef struct _CIs_ID3v2Tag
{
	char m_cTAG[3];
	unsigned char m_cVersion[2];
	unsigned char m_cFlags;
	unsigned char m_cSize_Encoded[4];
} CIs_ID3v2Tag;

typedef struct _CIs_ID3v2Frame
{
	char m_cFrameID[4];
	unsigned char m_cSize_Encoded[4];
	unsigned short m_cFlags;
} CIs_ID3v2Frame;
#pragma pack(pop)

struct xing
{
	long flags;
	unsigned long frames;
	unsigned long bytes;
	unsigned char toc[100];
	long scale;
};

enum { XING_FRAMES = 1, XING_BYTES = 2, XING_TOC = 4, XING_SCALE = 8 };

#define XING_MAGIC	(('X' << 24) | ('i' << 16) | ('n' << 8) | 'g')

struct dither
{
	mad_fixed_t error[3];
	mad_fixed_t random;
};

typedef struct
{
	HANDLE h;
	unsigned long size;				/* stream size, in bytes */

	struct xing xing;				/* Xing VBR tag data */

	struct mad_stream stream;		/* MAD stream structure */
	struct mad_frame frame;			/* MAD frame structure */
	struct mad_synth synth;			/* MAD synth structure */

	unsigned int samplecount;		/* samples output from current frame */

	mad_timer_t timer;				/* current playing time position */
	mad_timer_t length;				/* total playing time of current stream */
	unsigned long rate;				/* bitrate sum for computing average */
	unsigned long frames;			/* number of frames decoded */

	CPs_FileInfo info;

	unsigned char buffer[40960];	/* input stream buffer */
	DWORD buflen;					/* input stream buffer length */
} CPs_CoDec_MPEG;

BOOL MP3_OpenFile(CPs_CoDec_MPEG *context, LPCTSTR path);
void MP3_CloseFile(CPs_CoDec_MPEG *context);
void MP3_Seek(CPs_CoDec_MPEG *context, int const numer, int const denom);
BOOL MP3_GetPCMBlock(CPs_CoDec_MPEG *context, void *block, DWORD *size);
void MP3_GetFileInfo(CPs_CoDec_MPEG *context, CPs_FileInfo *info);
int MP3_GetCurrentPos(CPs_CoDec_MPEG *context);