6rooms.com's MP3 Source filter enables you playback MP3 files under DirectShow applications. Some non-standard MP3 files which cannot be played back by Windows own source filter are now supported.

6rooms.com's MP3 Source filter is distrubuted under GPLv3 license.

The source code can be compiled by Microsoft Visual C++ 2003 .NET.

MP3Source.dll in the binary distrubution is processed by a private utility so that MSVCRT.DLL version 6.0 can be used with. You can remove the post-build step script from the project settings.

DirectShow SDK BaseClasses and libmad-0.15b are required to compile the source code.

DirectShow SDK is not longer a part of DirectX SDK, it's included in Platform SDK now and is rewritten for VS2005. If you are still using VS2003 or VC6, we recommend you use DirectX SDK Summer 2004 edition (still available in MSDN website).

libmad-0.15b is available at http://www.underbit.com/products/mad/.