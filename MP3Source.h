/*
 * MP3Source - DirectShow MP3 Source Filter
 * Copyright (C) 2007 6rooms.com
 * Written by jh@6rooms.com.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#pragma once

#include "mp3dec.h"

class CMP3Stream;

DEFINE_GUID(CLSID_MP3Source, 0x3615c0de, 0x8ebe, 0x11ce, '6', 'r', 'o', 'o', 'm', 's', 0x7d, 0xa1);

class CMP3Source : public CSource, IFileSourceFilter, IAMFilterMiscFlags
{
public:
	static CUnknown * WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT *phr);
	DECLARE_IUNKNOWN;
	// Basic COM - used here to reveal our own interfaces
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void** ppv);

	// IFileSourceFilter
	STDMETHODIMP Load(LPCOLESTR pszFileName, const AM_MEDIA_TYPE *pmt);
	STDMETHODIMP GetCurFile(LPOLESTR *ppszFileName, AM_MEDIA_TYPE *pmt);

	// IAMFilterMiscFlags
	STDMETHODIMP_(ULONG) GetMiscFlags(void)
	{
		return AM_FILTER_MISC_FLAGS_IS_SOURCE;
	}

private:
	WCHAR m_szFileName[MAX_PATH];
	CMP3Source(LPUNKNOWN lpunk, HRESULT *phr);
};

class CMP3Stream : public CSourceStream, CSourceSeeking
{
public:
	CMP3Stream(HRESULT *phr, CMP3Source *pParent, LPCWSTR pPinName, LPCWSTR pszFileName);
	~CMP3Stream();
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void** ppv);

	HRESULT FillBuffer(IMediaSample *pms);
	HRESULT DecideBufferSize(IMemAllocator *pIMemAlloc, ALLOCATOR_PROPERTIES *pProperties);
	HRESULT GetMediaType(int iPosition, CMediaType *pMediaType);

	HRESULT CheckMediaType(const CMediaType* pmt)
	{
		if(pmt->majortype == MEDIATYPE_Audio
			&& pmt->subtype == MEDIASUBTYPE_PCM
			&& pmt->formattype == FORMAT_WaveFormatEx) return S_OK;

		return E_INVALIDARG;
	}
	HRESULT OnThreadStartPlay()
	{
		m_bDiscontinuity = TRUE;
		return DeliverNewSegment(m_rtStart, m_rtStop, m_dRateSeeking);
	}

	BOOL m_bDiscontinuity, m_bFlushing;
	void UpdateFromSeek();
	HRESULT ChangeStart();
	HRESULT ChangeStop();
	HRESULT ChangeRate() { return S_OK; }
	STDMETHODIMP SetRate(double dRate) { return E_NOTIMPL; }

private:
	CCritSec m_cSharedState;
	LPCWSTR m_pszFileName;
	LONGLONG m_rtPosition;

	CPs_CoDec_MPEG m_mp3;
	CPs_FileInfo m_mp3info;
};